FROM node:21

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

ENV port 8080
EXPOSE 8080

CMD [ "node", "app.js" ]