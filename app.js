const express = require('express')
const app = express()
const port = process.env.PORT || 8080 //for ansible setting
const { engine } = require('express-handlebars')
const routes = require('./routes')
const path = require('path')


app.engine('hbs', engine({ extname: 'hbs', defaultLayout: 'main' }))
app.set('view engine', 'hbs')
// app.use(express.static('public'))
app.use('/pos/public', express.static(path.join(__dirname, 'public')))

app.use('/pos', routes)
app.listen(port, () => {
  console.log(`app is running on http://localhost:${port}`)
})